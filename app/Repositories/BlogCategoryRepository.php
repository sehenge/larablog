<?php

namespace App\Repositories;

use App\Models\BlogCategory as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Support\Facades\Redis;

/**
 * Class BlogCategoryRepository
 * @package App\Repositories
 */
class BlogCategoryRepository extends CoreRepository
{
    /**
     * @return string
     */
    protected function getModelClass()
    {
        return Model::class;
    }

    /**
     * Get model for editing in admin panel
     *
     * @param int $id
     *
     * @return Model
     */
    public function getEdit($id)
    {
        return $this->startConditions()->find($id);
    }

    /**
     * Get list of categories for output in dropdown
     * @return Collection
     */
    public function getForComboBox()
    {
//        return $this->startConditions()->all();

        $columns = implode(', ', [
            'id',
            'CONCAT(id, ". ", title) AS new_title'
        ]);

        $result = $this->startConditions()
            ->selectRaw($columns)
            ->toBase()
            ->get();

        return $result;
    }

    public function getAllWithPaginate($perPage = null)
    {
        $columns = ['id', 'title', 'parent_id'];

        $result = $this
            ->startConditions()
            ->select($columns)
            ->with(['parentCategory:id,title'])
            ->paginate($perPage);

        return $result;
    }

//    public function getParentCategory($categoryId)
//    {
//        $columns = ['id', 'title'];
//        $result = $this
//            ->startConditions()
//            ->select($columns)
//            ->find($categoryId);
//
//        return $result;
//    }
}
