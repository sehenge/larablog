<?php

namespace App\Orchid\Screens;

use App\Orchid\Layouts\CategoryListLayout;
use Orchid\Screen\Screen;
use App\Models\BlogCategory as Category;

class CategoryListScreen extends Screen
{
    /**
     * Display header name.
     *
     * @var string
     */
    public $name = 'CategoryListScreen';

    /**
     * Display header description.
     *
     * @var string
     */
    public $description = 'All categories';

    /**
     * Query data.
     *
     * @return array
     */
    public function query(): array
    {
        return ['blog_categories' => Category::paginate(5)];
    }

    /**
     * Button commands.
     *
     * @return Action[]
     */
    public function commandBar(): array
    {
        return [];
    }

    /**
     * Views.
     *
     * @return Layout[]
     */
    public function layout(): array
    {
        return [CategoryListLayout::class];
    }
}
