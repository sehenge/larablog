<?php

namespace App\Orchid\Layouts;

use Orchid\Screen\Layouts\Table;
use Orchid\Screen\TD;
use App\Models\BlogCategory as Category;

class CategoryListLayout extends Table
{
    /**
     * Data source.
     *
     * @var string
     */
    protected $target = 'blog_categories';

    /**
     * @return TD[]
     */
    protected function columns(): array
    {
        return [
            TD::set('id', 'ID'),
            TD::set('title', 'Title'),
            TD::set('created_at', 'Created'),
            TD::set('updated_at', 'Last edit'),
        ];
    }
}
