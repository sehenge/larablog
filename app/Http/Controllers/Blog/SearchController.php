<?php

namespace App\Http\Controllers\Blog;

use App\Models\BlogPost;
use App\Events\SearchEvent;
use Illuminate\Http\Request;

class SearchController extends BaseController
{
    public function search(Request $request)
    {
        $query = $request->query('query');
        $products = BlogPost::where('title', 'like', '%' . $query . '%')
            ->get();

        event(new SearchEvent($products));
        return response()->json($products);
    }

    //fetch all products
    public function get(Request $request)
    {
        $query = $request->query('query');
//        return response()->json($request);
        $products = BlogPost::where('title', 'like', '%' . $query . '%')
            ->get();

        event(new SearchEvent($products));
        return response()->json($products);
    }

}
