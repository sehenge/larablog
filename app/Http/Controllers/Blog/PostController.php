<?php

namespace App\Http\Controllers\Blog;

use App\Models\BlogPost;
use App\Events\SearchEvent;
use App\Repositories\BlogCategoryRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use App\Repositories\BlogPostRepository;

class PostController extends BaseController
{
    /**
     * @var BlogPostRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $blogPostRepository;

    /**
     * @var BlogCategoryRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $blogCategoryRepository;


    /**
     * PostController constructor.
     */
    public function __construct()
    {
        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = request()->has('page') ? request()->get('page') : 1;

        $paginator = Cache::remember('post_paginator_' . $page, 15, function() {
            return $this->blogPostRepository->getAllWithPaginate(25);
        });

        return view('blog.posts.index', compact('paginator'));
    }

    public function search(Request $request)
    {
        $query = $request->query('query');
//        return response()->json($query);
        $products = BlogPost::where('title', 'like', '%' . $query . '%')
//            ->orWhere('description', 'like', '%' . $query . '%')
            ->get();

//        return response()->json($products);
        //broadcast search results with Pusher channels
        event(new SearchEvent($products));

//        return response()->json($products);
        return response()->json("ok");
    }

    public function sview()
    {
        return view('search');
    }

    //fetch all products
    public function get(Request $request)
    {
        $products = BlogPost::all();
        return response()->json($products);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
