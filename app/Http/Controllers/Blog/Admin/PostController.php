<?php

namespace App\Http\Controllers\Blog\Admin;

use App\Http\Requests\BlogPostCreateRequest;
use App\Http\Requests\BlogPostUpdateRequest;
use App\Models\BlogPost;
use App\Repositories\BlogCategoryRepository;
use App\Repositories\BlogPostRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Redis;
use App\Http\Requests;
use Illuminate\Support\Facades\Request;

class PostController extends BaseController
{

    /**
     * @var BlogPostRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $blogPostRepository;

    /**
     * @var BlogCategoryRepository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    private $blogCategoryRepository;

    /**
     * PostController constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware('auth');

        $this->blogPostRepository = app(BlogPostRepository::class);
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page = request()->has('page') ? request()->get('page') : 1;
//        if (!Redis::exists('posts:' . $page)) {
//            Redis::set('posts:' . $page, \Opis\Closure\serialize($this->blogPostRepository->getAllWithPaginate()));
//        }
//        $paginator = \Opis\Closure\unserialize(Redis::get('posts:' . $page));

        $paginator = Cache::remember('post_paginator_' . $page, 15, function() {
            return $paginator = $this->blogPostRepository->getAllWithPaginate(25);
        });
//        $paginator = $this->blogPostRepository->getAllWithPaginate();

        return view('blog.admin.posts.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $post = new BlogPost();
        $post = BlogPost::make();
        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.posts.edit', compact('post', 'categoryList'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BlogPostCreateRequest $request)
    {
        $data = $request->input();
//        $post = (new BlogPost())->create($data);
        $post = BlogPost::create($data);

        if ($post) {
            return redirect()->route('blog.admin.posts.edit', [$post->id])->with(['success' => 'Saved successfully']);
        } else {
            return back()->withErrors(['msg' => 'Error with saving'])->withInput();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        dd(__METHOD__, $id);
    }

    /**
     * @param int $id
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $post = $this->blogPostRepository->getEdit($id);
        if (empty($post)) {
            abort(404);
        }

        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.posts.edit',
            compact('post', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     */
    public function update(BlogPostUpdateRequest $request, $id)
    {
        $post = $this->blogPostRepository->getEdit($id);

        if (empty($post)) {
            return back()->withErrors(['msg' => "Record #{$id} not found"])->withInput();
        }

        $data = $request->all();

        /**
         * notice moved to observer @BlogPostObserver::setPublishedAt() | @BlogPostObserver::setSlug()
         */
//        if (empty($data['slug'])) {
//            $data['slug'] = Str::slug($data['title']);
//        }
//        if (empty($post->published_at) && $data['is_published']) {
//            $data['published_at'] = Carbon::now();
//        }

        $result = $post->update($data);

        if ($result) {
            return redirect()
                ->route('blog.admin.posts.edit', $post->id)
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()
                ->withErrors(['msg' => 'Error with saving'])
                ->withInput();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $result = BlogPost::destroy($id);
//        dd(__METHOD__, request()->all(), $id);

        if ($result) {
            return redirect()
                ->route('blog.admin.posts.index')
                ->with(['success' => "Record with id: {$id} was deleted"]);
        } else {
            return back()->withErrors(['msg' => 'Error with deleting']);
        }
    }
}
