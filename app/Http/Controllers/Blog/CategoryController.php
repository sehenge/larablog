<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Blog\Admin\BaseController as AdminBaseController;
use App\Http\Controllers\Blog\BaseController;
use App\Http\Requests\BlogCategoryCreateRequest;
use App\Http\Requests\BlogCategoryUpdateRequest;
use App\Models\BlogCategory;
use App\Repositories\BlogCategoryRepository;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class CategoryController extends BaseController
{
    /**
     * @var BlogCategoryRepository
     */
    private $blogCategoryRepository;

    public function __construct()
    {
        $this->blogCategoryRepository = app(BlogCategoryRepository::class);
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\Response
     * @throws \Psr\SimpleCache\InvalidArgumentException
     */
    public function index()
    {


        $page = request()->has('page') ? request()->get('page') : 1;

        $paginator = Cache::remember('category_paginator_' . $page, 15, function() {
            return $this->blogCategoryRepository->getAllWithPaginate(25);
        });

        return view('blog.categories.index', compact('paginator'));
    }

    /**
     * Show the form for creating a new resource.
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
//        $category = new BlogCategory();
        $category = BlogCategory::make();
//        $categoryList = BlogCategory::all();
        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.categories.edit', compact('category', 'categoryList'));
//        dd(__METHOD__);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(BlogCategoryCreateRequest $request)
    {
        $data = $request->input();
        if (empty($data['slug'])) {
            $data['slug'] = Str::slug($data['title']);
        }

//        $item = new BlogCategory($data);
        $item = BlogCategory::create($data);
        $item->save();

        //$item = (new BlogCategory())->create($data)); NOTE|NOTICE: another way to save model

        if ($item) {
            return redirect()->route('blog.admin.categories.edit', [$item->id])
                ->with(['success' => 'Successfully saved']);
        } else {
            return back()->withErrors(['msg' => 'Error with saving'])
                ->withInput();
        }
    }

    /**
     * @param                        $id
     * @param BlogCategoryRepository $categoryRepository
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
//        $category = BlogCategory::findOrFail($id);
//        $categoryList = BlogCategory::all();

        $category = $this->blogCategoryRepository->getEdit($id);
        if (empty($category)) {
            abort(404);
        }
        $categoryList = $this->blogCategoryRepository->getForComboBox();

        return view('blog.admin.categories.edit',
            compact('category', 'categoryList'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BlogCategoryUpdateRequest $request
     * @param int                      $id
     *
     * @return \Illuminate\Http\Response
     * @throws \Illuminate\Validation\ValidationException
     */
    public function update(BlogCategoryUpdateRequest $request, $id)
    {
//        $validatedData = $this->validate($request, $rules);
//        dd($validatedData);
//        $item = BlogCategory::find($id);
        $item = $this->blogCategoryRepository->getEdit($id);

        if (empty($item) || is_null($item)) {
            return back()
                ->withErrors(['msg' => "Record with id: {$id} not found"])
                ->withInput();
        }

        $data = $request->all();

        //notice moved to observer
//        if (empty($data['slug'])) {
//            $data['slug'] = Str::slug($data['title']);
//        }

        $result = $item->update($data);
//            ->fill($data)
//            ->save();

        if ($result) {
            return redirect()
                ->route('blog.admin.categories.edit', $item->id)
                ->with(['success' => 'Saved successfully']);
        } else {
            return back()
                ->withErrors(['msg' => "Error with saving"])
                ->withInput();
        }
    }
}
