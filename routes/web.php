<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
//Route::get('/', 'Blog\PostController@index')->name('blog');  //todo: check why FORBIDDEN
Route::get('/', 'Blog\PostController@sview')->name('search');


Route::group(['namespace' => 'Blog', 'prefix' => 'blog'], function() {
//    $methods = ['index', 'edit', 'update', 'create', 'store'];
    Route::resource('posts', 'PostController')->names('blog.posts');
    Route::resource('categories', 'CategoryController')->names('blog.categories');
});

//Route::resource('rest', 'RestTestController')->names('restTest');

Route::get('/home', 'Blog\PostController@index')->name('home');

Route::group(['prefix' => 'digging_deeper'], function() {
    Route::get('collections', 'DiggingDeeperController@collections')
        ->name('digging_deeper.collections');
});

$groupData = [
    'namespace' => 'Blog\Admin',
    'prefix'    => 'admin/blog'
];
Route::group($groupData, function() {
    $methods = ['index', 'edit', 'update', 'create', 'store'];
    Route::resource('categories', 'CategoryController')
        ->only($methods)
        ->names('blog.admin.categories');

    Route::resource('posts', 'PostController')
        ->except(['show'])
        ->names('blog.admin.posts');
});

/*routes for News*/
//Route::group(['prefix' => 'news'], function () {
//    Route::get('data', 'NewsController@data')->name('news.data');
//    Route::get('{news}/delete', 'NewsController@destroy')->name('news.delete');
//    Route::get('{news}/confirm-delete', 'NewsController@getModalDelete')->name('news.confirm-delete');
//});
//Route::resource('news', 'NewsController');
