@extends('layouts.frontend.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-12">
                <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
                    <a class="btn btn-primary" href="{{route('blog.admin.categories.create')}}">Add category</a>
                </nav>
                <div class="card">
{{--                    <div class="card-header">Dashboard</div>--}}

                    <div class="card-body">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Category</th>
                                <th>Parent</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($paginator as $category)
                                @php /** @var \App\Models\BlogCategory @category */ @endphp
                                <tr>
                                    <td>{{$category->id}}</td>
                                    <td>
                                        <a href="{{route('blog.admin.categories.edit', $category->id)}}">
                                            {{$category->title}}
                                        </a>
                                    </td>
                                    <td @if(in_array($category->parent_id, [0, 1])) style="color: #ccc;" @endif>
{{--                                        {{$category->parent_id}}--}}
{{--                                        {{$category->parentCategory->title ?? '?'}} //notice: the same 1--}}
{{--                                        {{optional($category->parentCategory)->title}} //notice: the same 1--}}

                                        {{-- notice: other variant 2--}}
{{--                                        {{--}}
{{--                                            $category->parentCategory->title--}}
{{--                                                ?? ($category->id === \App\Models\BlogCategory::ROOT--}}
{{--                                                    ? 'Root'--}}
{{--                                                    : '???')--}}
{{--                                        }}--}}
                                        {{-- notice: accessors/mutators--}}

                                        {{$category->parentTitle}}


                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        @if($paginator->total() > $paginator->count())
            <br />
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="card">
                         <div class="card-body">
                            {{$paginator->links()}}
                         </div>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection
