@php
    /** @var \App\Models\BlogPost $post */
@endphp
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    @if($post->is_published)
                        Published
                    @else
                        Draft
                    @endif
                </div>
                <div class="card-body">
                    <div class="card-title"></div>
                    <div class="card-subtitle mb-2 text-muted"></div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Main data</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#adddata" role="tab">Additional data</a>
                        </li>
                    </ul>
                    <br />
                    <div class="tab-content">
                        <div class="tab-pane active" id="maindata" role="tabpanel">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input name="title" value="{{old('title', $post->title)}}"
                                       id="title"
                                       type="text"
                                       class="form-control"
                                       minlength="3"
                                       required />
                            </div>
                            <div class="form-group">
                                <label for="content_raw">Article</label>
                                <textarea name="content_raw"
                                          id="content_raw"
                                          class="form-control"
                                          rows="20">{{old('content_raw', $post->content_raw)}}</textarea>
                            </div>
                        </div>
                        <div class="tab-pane" id="adddata" role="tabpanel">
                            <div class="form-group">
                                <label for="category">Category</label>
                                <select name="category_id"
                                        id="category"
                                        class="form-control"
                                        placeholder="Choose category"
                                        required>
                                    <option value="" disabled selected>Select category</option>
                                    @foreach($categoryList as $categoryOption)
                                        <option value="{{$categoryOption->id}}"
                                                @if($categoryOption->id == $post->category_id) selected @endif>
                                            {{$categoryOption->new_title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="slug">Identificator</label>
                                <input name="slug" value="{{$post->slug}}"
                                       id="slug"
                                       type="text"
                                       class="form-control">
                            </div>
                            <div class="form-group">
                                <label for="excerpt">Excerpt</label>
                                <textarea name="excerpt"
                                          id="excerpt"
                                          class="form-control"
                                          rows="3">{{old('excerpt', $post->excerpt)}}</textarea>
                            </div>
                            <div class="form-check">
                                <input name="is_published" type="hidden" value="0">
                                <input name="is_published"
                                    type="checkbox"
                                    class="form-check-input"
                                    value="1"
                                    @if($post->is_published)
                                        checked="checked"
                                    @endif
                                >
                                <label class="form-check-label" for="is_published">Published</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
