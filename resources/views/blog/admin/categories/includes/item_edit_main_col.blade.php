@php
    /** @var \App\Models\BlogCategory $category */
    /** @var \Illuminate\Database\Eloquent\Collection $categoryList */
@endphp
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="card-title"></div>
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" data-toggle="tab" href="#maindata" role="tab">Main data</a>
                        </li>
                    </ul>
                    <br />
                    <div class="tab-content">
                        <div class="tab-pane active" id="maindata" role="tabpanel">
                            <div class="form-group">
                                <label for="title">Title</label>
                                <input name="title" value="{{old('title', $category->title)}}"
                                       id="title"
                                       type="text"
                                       class="form-control"
                                       minlength="3"
                                       required />
                            </div>
                            <div class="form-group">
                                <label for="slug">Slug</label>
                                <input name="slug" value="{{$category->slug}}"
                                       id="slug"
                                       type="text"
                                       class="form-control" />
                            </div>
                            <div class="form-group">
                                <label for="parent_id">Parent</label>
                                <select name="parent_id"
                                        id="title"
                                        class="form-control"
                                        required>
                                    <option value="" disabled selected>Select category</option>
                                    @foreach($categoryList as $categoryOption)
                                        <option value="{{$categoryOption->id}}"
                                            @if($categoryOption->id == $category->parent_id) selected @endif>
{{--                                            {{$categoryOption->id}}. {{$categoryOption->title}}--}}
                                            {{$categoryOption->new_title}}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="description">Description</label>
                                <textarea name="description"
                                          id="description"
                                          class="form-control"
                                          rows="3">{{old('description', $category->description)}}</textarea>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
