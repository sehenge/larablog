@php
    /** @var \App\Models\BlogCategory $category */
@endphp
<div class="row justify-content-center">
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <button type="submit" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
<br />
@if($category->exists)
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <ul class="list-unstyled" style="margin: 0;">
                        <li>ID: {{$category->id}}</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <br />
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-body">
                    <div class="form-group">
                        <label for="created">Created</label>
                        <input name="created" type="text" value="{{$category->created_at}}" class="form-control" disabled />
                    </div>
                    <div class="form-group">
                        <label for="updated">Updated</label>
                        <input name="updated" type="text" value="{{$category->updated_at}}" class="form-control" disabled />
                    </div>
                    <div class="form-group">
                        <label for="deleted">Deleted</label>
                        <input name="deleted" type="text" value="{{$category->deleted_at}}" class="form-control" disabled />
                    </div>
                </div>
            </div>
        </div>
    </div>
@endif
