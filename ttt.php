<?php
function longestCommonParts($words)
{
    $words = array_map('strtolower', array_map('trim', $words));

    $longest_common_parts = array();
    $shortest_string = strcmp($words[0], $words[1]) ? $words[0] : $words[1];
    $shortest_string = str_split($shortest_string);

    while (sizeof($shortest_string)) {
        array_unshift($longest_common_parts, '');
        foreach ($shortest_string as $char) {
            foreach ($words as $word) {
                if (!strstr($word, $longest_common_parts[0] . $char)) break 2;
            }
            $longest_common_parts[0] .= $char;
        }
        array_shift($shortest_string);
    }

    $maxLength = max(array_map('strlen', $longest_common_parts));
    $longest_substrings = [];
    foreach ($longest_common_parts as $id => $substring) {
        if (strlen($substring) == $maxLength) {
            $longest_substrings[] = $longest_common_parts[$id];
        }
    }
    return $longest_substrings;
}
$array1 = array(
    'Doors',
    'Kangaroo'
);

$array2 = array(
    'Doorskkooo',
    'Kangarookk'
);
$longest_common_parts1 = array_unique(longestCommonParts($array1));
$longest_common_parts2 = array_unique(longestCommonParts($array2));

var_dump($longest_common_parts1);
var_dump($longest_common_parts2);
